#-------------------------------------------------
#
# Project created by QtCreator 2011-11-11T16:50:07
#
#-------------------------------------------------

QT       += core widgets gui network

TARGET = dualHDGrab
TEMPLATE = app
INCLUDEPATH += /home/jhowland/hg/dualhdgrab/BlackMagicSDK/Linux/include

INCLUDEPATH += /usr/include
LIBS += -lopencv_core\
        -lopencv_imgproc\
        -lopencv_highgui \
        -ltiff

SOURCES += main.cpp\
        HdGrabMainWindow.cpp \
        ini_file.cpp \
        /home/jhowland/hg/dualhdgrab/BlackMagicSDK/Linux/include/DeckLinkAPIDispatch.cpp \
    AdvancedWidget.cpp

HEADERS  += HdGrabMainWindow.h \
    ini_file.h \
    compilation.h \
    AdvancedWidget.h

