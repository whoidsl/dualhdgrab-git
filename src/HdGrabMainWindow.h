#ifndef HDGRABMAINWINDOW_H
#define HDGRABMAINWINDOW_H
#include <libgen.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <QtGui>
#include <QtNetwork>
#include "opencv2/core/core.hpp"
#include "opencv2/core/mat.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "tiffio.h"




#include "ini_file.h"
#include "DeckLinkAPI.h"

#include "AdvancedWidget.h"

#define DEFAULT_DATA_DIRECTORY "/data/"
#define DEFAULT_GRAB_INTERVAL 0

#define PROGRAM_VERSION "1.0"
#define DECKLINK_INITIALIZATION_OK  0
#define NO_DECKLINK_DRIVERS         -100
#define NO_DECKLINK_BOARD           -101
#define NO_SECOND_DECKLINK_BOARD    -201

#define BAD_QUERY           -102

#define MAX_IMAGE_ROW   5760

#define                       DEFAULT_IN_SOCKET    63000
#define                       BAD_VV_ADDRESS       "198.17.154.254"

class DeckLinkCaptureDelegate : public IDeckLinkInputCallback
{
public:
    virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, LPVOID *ppv) { return E_NOINTERFACE; }
    virtual ULONG STDMETHODCALLTYPE AddRef(void) { return 1; }
    virtual ULONG STDMETHODCALLTYPE  Release(void) { return 1; }
    virtual HRESULT STDMETHODCALLTYPE VideoInputFormatChanged(BMDVideoInputFormatChangedEvents, IDeckLinkDisplayMode*, BMDDetectedVideoInputFormatFlags);
    virtual HRESULT STDMETHODCALLTYPE VideoInputFrameArrived(IDeckLinkVideoInputFrame*, IDeckLinkAudioInputPacket*);
    HRESULT setGrabFlag(bool flag);
    void    setInterface(int theInterface);
    void    setDescriptor(QString descriptorString);
    void    setDataDirectory(char *dString);
private:
    bool    grabFlag;
    int     imageFrames;
    int     interface;
    char    descriptor[768];
    char    *directoryName;
};


class HDGrabMainWindow : public QWidget
{
    Q_OBJECT

protected:
   void closeEvent(QCloseEvent *event);

public:
    HDGrabMainWindow(char	*startup_file_name);
    ~HDGrabMainWindow();
    void                   imageSaved(char *imageName, int channel);
    QImage                 *imageView1;
    QImage                 *imageView2;
    void                   setUpdateFlag(bool updateFlag);
    void                   setAutoGrabRate(int rate);



private:
    IniFile                   iniFile;
    int                       grabInterval;
    int                       readIniFile();
    int                       connectToBoard();
    bool                      updateNow;
    bool                      foundBoards;
    void                      updateImage();
    QTimer                    *updateTimer;
    QTimer                    *grabTimer;
    AdvancedWidget           *advancedWidget;

    QLabel                 *imageArea1;

    QLabel                 *imageArea2;

    QPushButton            *showAdvancedButton;

    QPushButton            *grabButton;
    QPushButton            *grabLeftButton;
    QPushButton            *grabRightButton;


    QGroupBox              *leftGrabFrame;
    QGroupBox              *rightGrabFrame;


    bool                 OKToClose();


    IDeckLink*					deckLink1;
    DeckLinkCaptureDelegate                     *delegate1;

    IDeckLinkInput                              *deckLinkInput1;

    IDeckLink*					deckLink2;
    DeckLinkCaptureDelegate                     *delegate2;

    IDeckLinkInput                              *deckLinkInput2;

    QUdpSocket             *listenSocket;
    QUdpSocket             *dataSocket;
    unsigned short         dataPort;
    unsigned short         alertPort;
    unsigned short         vvPort;
    QHostAddress           vvAddress;
    bool                   vvAddressisGood;

    bool                   grabLeft;
    bool                   grabRight;
    char                   myCruiseID[256];
    char                   myLoweringID[256];
    char                   *dataDirectory;


 private slots:
    void                   acceptHDGrabSocketInput();
    void                   imageGrabTime();
    void                   listenForData();
    void                   grabFunction();
    void                   grabLeftFunction();
    void                   grabRightFunction();
    void                   imageUpdateTime();
};


extern HDGrabMainWindow *mainWindow;
#endif // HDGRABMAINWINDOW_H
