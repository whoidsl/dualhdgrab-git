#include <QtGui>
#include "HdGrabMainWindow.h"

HDGrabMainWindow    *mainWindow;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    char	*enteredIniFileName;
    if(argc > 1)
    {
        enteredIniFileName = strdup((char *)argv[1]);
    }
    else
    {

        enteredIniFileName = strdup("./hdGrab.ini");

    }






    mainWindow = new HDGrabMainWindow(enteredIniFileName);

    char	windowTitle[256];
    sprintf(windowTitle,"hdGrab V %s compiled %s",PROGRAM_VERSION, __DATE__);
    mainWindow->setWindowTitle(windowTitle);
    mainWindow->show();

    return app.exec();
}
