#include "HdGrabMainWindow.h"

// 24 July 2014, swithc red and blue to make them right. jhowland
// 6 august 2014 make attempt at video format detection
using namespace cv;

int
unix_mkpath (const char *s, mode_t mode)
{
   char *q, *r = NULL, *path = NULL, *up = NULL;
   int rv;

   rv = -1;
   if (strcmp (s, ".") == 0 || strcmp (s, "/") == 0)
      return (0);

   if ((path = strdup (s)) == NULL)
      exit (EXIT_SUCCESS);

   if ((q = strdup(s)) == NULL)
      exit (EXIT_SUCCESS);

   if ((r = dirname (q)) == NULL)
      goto out;

   if ((up = strdup (r)) == NULL)
      exit (EXIT_SUCCESS);

   if ((unix_mkpath (up, mode) == -1) && (errno != EEXIST))
      goto out;

   if ((mkdir (path, mode) == -1) && (errno != EEXIST))
      rv = -1;
   else
      rv = 0;

out:
   if (up != NULL)
      free (up);
   free (q);
   free (path);
   return (rv);
}


extern HDGrabMainWindow *mainWindow;
IDeckLinkInput *firstDeckLinkInput;
IDeckLinkInput *secondDeckLinkInput;


HRESULT DeckLinkCaptureDelegate::setGrabFlag(bool flag)
{
   grabFlag = flag;
   imageFrames = 0;
   return S_OK;
};

void DeckLinkCaptureDelegate::setInterface(int theInterface)
{
   interface = theInterface;
}

void DeckLinkCaptureDelegate::setDescriptor(QString descriptorString)
{
   QString myDescriptorString = "interface " + QString::number(interface) + " " + descriptorString;
   strncpy(descriptor, descriptorString.toLatin1().data(),768);
}

void DeckLinkCaptureDelegate::setDataDirectory(char *directoryString)
{
   directoryName = strdup(directoryString);
}



HRESULT DeckLinkCaptureDelegate::VideoInputFrameArrived(IDeckLinkVideoInputFrame* videoFrame, IDeckLinkAudioInputPacket* audioFrame)
{
   void *frameBytes;

   // Handle Video Frame
   if(videoFrame)
      {
         if(grabFlag)
            {
               if (videoFrame->GetFlags() & bmdFrameHasNoInputSource)
                  {
                     fprintf(stderr, "Frame received - No input signal detected\n" );
                  }
               else
                  {
                     fprintf(stderr, "Frame received - Valid Frame (Size: %li bytes)\n", videoFrame->GetRowBytes() * videoFrame->GetHeight());

                     Mat frame;
                     Mat outFrame;
                     Mat halfFrame;
                     videoFrame->GetBytes(&frameBytes);
                     BMDPixelFormat theFormat = videoFrame->GetPixelFormat();
                     unsigned char *theByte = (unsigned char *)frameBytes;
                     int theRows = videoFrame->GetHeight();
                     int theCols = videoFrame->GetWidth();
                     switch(theFormat)
                        {
                        case bmdFormat8BitYUV:
                           {
                              frame.create(theRows, theCols, CV_8UC3);

                              for(int row=0; row < theRows; row++)
                                 {
                                    int rowOffset = row*videoFrame->GetRowBytes();
                                    for(int col = 0; col < theCols; col+=2)
                                       {
                                          unsigned char *word1 = (unsigned char *)frameBytes + rowOffset + (col * 2);
                                          unsigned char *word2 = (unsigned char *)frameBytes + rowOffset + (col * 2) + 2;
                                          unsigned char u = *word1;
                                          unsigned char y1 = *(word1+1);
                                          unsigned char v = *word2;
                                          unsigned char y2 = *(word2+1);


                                          frame.at<cv::Vec3b>(row,col)[0]=y1;
                                          frame.at<cv::Vec3b>(row,col)[1]=u;
                                          frame.at<cv::Vec3b>(row,col)[2]=v;
                                          //fprintf(stderr, "y,u,v %d %d %d\n",y1,u,v );
                                          frame.at<cv::Vec3b>(row,col+1)[0]=y2;
                                          frame.at<cv::Vec3b>(row,col+1)[1]=u;
                                          frame.at<cv::Vec3b>(row,col+1)[2]=v;




                                       }
                                 }


                              char filename2[64];
                              QString theFilenameBase = QDateTime::currentDateTime().toString("yyyyMMddhhmmss.tif");

                              QString theCaptureDirectoryName = (QString)directoryName +"/" +  "capture_" + QDateTime::currentDateTime().toString("yyyyMMdd");
                              unix_mkpath(theCaptureDirectoryName.toLatin1().data(),0775);

                              //QString yuvFileName = "YUV." + theFilenameBase;
                              QString rgbFileName = theCaptureDirectoryName + "/cam" +  QString::number(interface) +  "_" + theFilenameBase;

                              //sprintf(filename1,"%s",yuvFileName.toAscii().data());
                              sprintf(filename2,"%s",rgbFileName.toLatin1().data());
                              //cv::cvtColor(frame,outFrame,cv::COLOR_YCrCb2RGB);
                              cv::cvtColor(frame,outFrame,cv::COLOR_YUV2RGB);
                              //cv::imwrite(filename1,frame);
                              TIFF *image;
                              unsigned char	out_strip[MAX_IMAGE_ROW];

                              if((image = TIFFOpen(filename2, "w")) == NULL){
                                    return 0;
                                 }
                              // We need to set some values for basic tags before we can add any data
                              int rows = theRows;
                              int columns = theCols;
                              TIFFSetField(image, TIFFTAG_IMAGEWIDTH, columns);
                              TIFFSetField(image, TIFFTAG_IMAGELENGTH, rows);
                              TIFFSetField(image, TIFFTAG_BITSPERSAMPLE, 8);
                              TIFFSetField(image, TIFFTAG_SAMPLESPERPIXEL, 3);
                              TIFFSetField(image, TIFFTAG_ROWSPERSTRIP, theRows);
                              TIFFSetField(image, TIFFTAG_PHOTOMETRIC,  PHOTOMETRIC_MINISBLACK);
                              TIFFSetField(image, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
                              TIFFSetField(image, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
                              TIFFSetField(image, TIFFTAG_PHOTOMETRIC,  PHOTOMETRIC_RGB);
                              TIFFSetField(image, TIFFTAG_XRESOLUTION, 1.0);
                              TIFFSetField(image, TIFFTAG_YRESOLUTION, 1.0);
                              TIFFSetField(image, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);
                              TIFFSetField(image, TIFFTAG_IMAGEDESCRIPTION,descriptor);
                              // Write the information to the file


                              for(int tifrow = 0; tifrow < rows; tifrow++)
                                 {
                                    for(int tifcol = 0; tifcol < columns; tifcol++)
                                       {
                                          out_strip[3*tifcol] =   outFrame.at<cv::Vec3b>(tifrow,tifcol)[2];
                                          out_strip[3*tifcol+1] = outFrame.at<cv::Vec3b>(tifrow,tifcol)[1];
                                          out_strip[3*tifcol+2] = outFrame.at<cv::Vec3b>(tifrow,tifcol)[0];

                                       }
                                    TIFFWriteScanline(image, out_strip, (uint32 )tifrow,0);
                                 }
                              // Close the file
                              TIFFClose(image);
                              mainWindow->imageSaved(filename2,interface);

                              cv::resize(outFrame,halfFrame,cv::Size(),.125,.125);

                              for(int row = 0; row < halfFrame.rows; row++)
                                 {
                                    for(int col = 0; col < halfFrame.cols; col++)
                                       {
                                          QColor	thisColor;
                                          unsigned char blue = halfFrame.at<cv::Vec3b>(row,col)[0];
                                          unsigned char green = halfFrame.at<cv::Vec3b>(row,col)[1];
                                          unsigned char red = halfFrame.at<cv::Vec3b>(row,col)[2];
                                          thisColor.setRgb(red,green,blue);
                                          if(1 == interface)
                                             {
                                                mainWindow->imageView1->setPixel(col,row,thisColor.rgb()); // note the change in row/column
                                             }
                                          else
                                             {
                                                mainWindow->imageView2->setPixel(col,row,thisColor.rgb()); // note the change in row/column
                                             }
                                       }
                                 }
                              mainWindow->setUpdateFlag(true);
                              frame.release();
                              outFrame.release();
                              halfFrame.release();

                              imageFrames++;
                              break;
                           }

                        case bmdFormat10BitYUV:
                           {
                              break;
                           }
                        case bmdFormat8BitARGB:
                           {
                              break;
                           }
                        case bmdFormat8BitBGRA:
                           {
                              break;
                           }
                        case bmdFormat10BitRGB:
                           {
                              break;
                           }

                        }

                     grabFlag = false;

                  }
            }
      }

   return S_OK;
}

HRESULT DeckLinkCaptureDelegate::VideoInputFormatChanged(BMDVideoInputFormatChangedEvents events, IDeckLinkDisplayMode *mode, BMDDetectedVideoInputFormatFlags)
{
   // This only gets called if bmdVideoInputEnableFormatDetection was set
   // when enabling video input
   HRESULT	result;
   char*	displayModeName = NULL;

   if (!(events & bmdVideoInputDisplayModeChanged))
      {
         return S_OK;
      }

   mode->GetName((const char**)&displayModeName);
   printf("Video format changed to %s\n", displayModeName);

   if (displayModeName)
      {
         free(displayModeName);
      }

   if (1 == interface)
      {
         firstDeckLinkInput->StopStreams();

         result = firstDeckLinkInput->EnableVideoInput(mode->GetDisplayMode(), bmdFormat8BitYUV, bmdVideoInputEnableFormatDetection);
         if (result != S_OK)
            {
               fprintf(stderr, "Failed to switch video mode interface 1\n");
               goto bail;
            }

         firstDeckLinkInput->StartStreams();
      }
   else
      {

         secondDeckLinkInput->StopStreams();

         result = secondDeckLinkInput->EnableVideoInput(mode->GetDisplayMode(), bmdFormat8BitYUV, bmdVideoInputEnableFormatDetection);
         if (result != S_OK)
            {
               fprintf(stderr, "Failed to switch video mode interface 2\n");
               goto bail;
            }

         secondDeckLinkInput->StartStreams();
      }


bail:
   return S_OK;
}


HDGrabMainWindow::HDGrabMainWindow(char	*startup_file_name)
   : QWidget()
{
   grabLeft = true;
   grabRight = true;
   vvAddressisGood= false;
   int	startup_return_code = iniFile.open_ini(startup_file_name);
   if(startup_return_code != GOOD_INI_FILE_READ)
      {
         return ;
      }
   startup_return_code = readIniFile();

   listenSocket = new QUdpSocket(this);
   listenSocket->bind(alertPort);
   connect(listenSocket, SIGNAL(readyRead()), this, SLOT(acceptHDGrabSocketInput()));

   dataSocket = new QUdpSocket(this);
   dataSocket->bind(dataPort);
   connect(dataSocket, SIGNAL(readyRead()), this, SLOT(listenForData()));

   advancedWidget = new AdvancedWidget();

   int boardSuccess = connectToBoard();
   if(2 != boardSuccess)
      {
         QMessageBox::critical(this, "did not find two interfaces.","reboot and try again?");

      }

   imageArea1 = new QLabel();
   imageArea1->setFixedHeight(135);
   imageArea1->setFixedWidth(240);
   imageArea1->setFrameStyle(QFrame::StyledPanel);

   imageView1 = new QImage(240,135,QImage::Format_RGB32);

   imageArea2 = new QLabel();
   imageArea2->setFixedHeight(135);
   imageArea2->setFixedWidth(240);
   imageArea2->setFrameStyle(QFrame::StyledPanel);

   imageView2 = new QImage(240,135,QImage::Format_RGB32);

   leftGrabFrame = new QGroupBox("image 1");
   rightGrabFrame = new QGroupBox("image 2");


   grabLeftButton  = new QPushButton("grab");
   connect(grabLeftButton,SIGNAL(clicked()), this, SLOT(grabLeftFunction()));

   grabRightButton = new QPushButton("grab");
   connect(grabRightButton,SIGNAL(clicked()), this, SLOT(grabRightFunction()));

   grabButton = new QPushButton("grab both");
   connect(grabButton,SIGNAL(clicked()), this, SLOT(grabFunction()));

   QVBoxLayout *mainLayout = new QVBoxLayout(this);
   QHBoxLayout *imageLayout = new QHBoxLayout();

   QHBoxLayout *leftlayout = new QHBoxLayout();
   leftGrabFrame->setLayout(leftlayout);

   QHBoxLayout *rightlayout = new QHBoxLayout();
   rightGrabFrame->setLayout(rightlayout);

   leftlayout->addWidget(grabLeftButton);
   rightlayout->addWidget(grabRightButton);


   QHBoxLayout *leftRightLayout = new QHBoxLayout();

   leftRightLayout->addWidget(leftGrabFrame);
   leftRightLayout->addWidget(rightGrabFrame);

   imageLayout->addWidget(imageArea1);
   imageLayout->addWidget(imageArea2);
   mainLayout->addLayout(imageLayout);
   mainLayout->addLayout(leftRightLayout);
   mainLayout->addWidget(grabButton);

   showAdvancedButton = new QPushButton("Advanced...");
   connect(showAdvancedButton, SIGNAL(clicked()), advancedWidget, SLOT(show()));
   QHBoxLayout *advlayout = new QHBoxLayout();
   advlayout->addSpacing(200);
   advlayout->addWidget(showAdvancedButton);
   mainLayout->addLayout(advlayout);

   updateNow = false;

   updateTimer = new QTimer();
   connect(updateTimer,SIGNAL(timeout()), this, SLOT(imageUpdateTime()));
   updateTimer->start(50);

   grabTimer = new QTimer();
   connect(grabTimer,SIGNAL(timeout()), this, SLOT(imageGrabTime()));
   if(grabInterval)
      {
         grabTimer->start(1000*grabInterval);
      }




}

void HDGrabMainWindow::imageGrabTime()
{
   grabFunction();
}

void HDGrabMainWindow::imageSaved(char *imageName, int channel)
{
   QString currentTime = QDateTime::currentDateTimeUtc().toString("yyyy/MM/DD hh:mm:ss.zzz");
   char outString[512];
   int len = snprintf(outString,512, "IMGR %s CHAN=%d %s ",currentTime.toLatin1().data(),channel,imageName);
   if(vvAddressisGood)
      {
         listenSocket->writeDatagram(outString, len, vvAddress,vvPort);
      }
}



void HDGrabMainWindow::imageUpdateTime()
{
   if(updateNow)
      {
         updateImage()   ;
         updateNow = false;
      }
}

void HDGrabMainWindow::setUpdateFlag(bool updateMe)
{
   updateNow = updateMe;
}

void HDGrabMainWindow::updateImage()
{
   imageArea1->setPixmap(QPixmap::fromImage(*(mainWindow->imageView1)));
   imageArea2->setPixmap(QPixmap::fromImage(*(mainWindow->imageView2)));
}

void HDGrabMainWindow::grabFunction()
{
   if(foundBoards)
      {
         if(grabLeft)
            {
               if(delegate1)
                  {
                     delegate1->setGrabFlag(true);

                  }
            }
         if(grabRight)
            {
               if(delegate2)
                  {
                     delegate2->setGrabFlag(true);

                  }
            }
      }

}

void HDGrabMainWindow::grabLeftFunction()
{
   if(foundBoards)
      {
         if(grabLeft)
            {
               if(delegate1)
                  {
                     delegate1->setGrabFlag(true);

                  }
            }
      }

}

void HDGrabMainWindow::grabRightFunction()
{
   if(foundBoards)
      {
         if(grabRight)
            {
               if(delegate2)
                  {
                     delegate2->setGrabFlag(true);

                  }
            }
      }

}

void HDGrabMainWindow::setAutoGrabRate(int theRate)
{
   grabInterval = theRate;
   if(theRate > 0)
      {
         grabTimer->start(1000*grabInterval);

      }
   else
      {
         grabTimer->stop();
      }
}

int   HDGrabMainWindow::readIniFile()
{
   alertPort =   iniFile.read_int("GENERAL", "INCOMING_SOCKET", DEFAULT_IN_SOCKET);
   dataPort =   iniFile.read_int("GENERAL", "DATA_SOCKET", DEFAULT_IN_SOCKET+1);
   vvPort =   iniFile.read_int("GENERAL", "EVENTLOGGER_PORT", DEFAULT_IN_SOCKET);
   grabInterval = iniFile.read_int("GENERAL", "AUTOGRAB_INTERVAL", DEFAULT_GRAB_INTERVAL);
   char  *scratchString = iniFile.read_string("GENERAL","EVENTLOGGER_ADDRESS", BAD_VV_ADDRESS);
   if(strncmp(scratchString,BAD_VV_ADDRESS,strlen(BAD_VV_ADDRESS)) )
      {
         vvAddress.setAddress((QString) scratchString);
         vvAddressisGood = true;
      }
   free(scratchString);
   dataDirectory = iniFile.read_string("GENERAL","DATA_DIRECTORY",(char *)DEFAULT_DATA_DIRECTORY);

   return GOOD_INI_FILE_READ;
}

void HDGrabMainWindow::listenForData()
{
   char cruiseID[256];
   char loweringID[256];
   while(dataSocket->hasPendingDatagrams())
      {

         QByteArray	buffer(dataSocket->pendingDatagramSize(),0);
         dataSocket->readDatagram(buffer.data(),buffer.size());

         if(!strncmp(buffer.data(),"ODR",3))
            {
               int items = sscanf(buffer.data(),"%*s %*s %*s %*s %*s %*s %*s %s %s",cruiseID,loweringID);
               if(2 == items)
                  {
                     strncpy(myCruiseID,cruiseID,256);
                     strncpy(myLoweringID,loweringID,256);
                     QString titleString = (QString)myCruiseID + ":" + (QString)myCruiseID;

                     setWindowTitle(titleString);
                  }
            }
      }

}

void HDGrabMainWindow::acceptHDGrabSocketInput()
{
   while(listenSocket->hasPendingDatagrams())
      {

         QByteArray	buffer(listenSocket->pendingDatagramSize(),0);
         listenSocket->readDatagram(buffer.data(),buffer.size());

         if(!strncmp(buffer.data(),"GRABHD",6))
            {
               if(foundBoards)

                  if(delegate1)
                     {
                        delegate1->setGrabFlag(true);
                     }
               if(delegate2)
                  {
                     delegate2->setGrabFlag(true);
                  }
            }
      }


}

HDGrabMainWindow::~HDGrabMainWindow()
{

}

int HDGrabMainWindow::connectToBoard()
{

   foundBoards = false;
   IDeckLinkIterator*	deckLinkIterator = NULL;
   bool	success = false;
   int						numDevices = 0;
   HRESULT						result;
   // **** Find a DeckLink instance and obtain video output interface
   deckLinkIterator = CreateDeckLinkIteratorInstance();
   if (deckLinkIterator == NULL)
      {
         QMessageBox::critical(this, "This application requires the DeckLink drivers installed.", "Please install the Blackmagic DeckLink drivers to use the features of this application.");
         return(NO_DECKLINK_DRIVERS);
      }
   // Connect to the first DeckLink instance


   if (deckLinkIterator->Next(&deckLink1) != S_OK)
      {
         QMessageBox::critical(this, "This application requires a DeckLink dual PCI card.", "You will not be able to use the features of this application until a DeckLink PCI card is installed.");
         return(NO_DECKLINK_BOARD);
      }

   if (deckLink1->QueryInterface(IID_IDeckLinkInput, (void**)&deckLinkInput1) != S_OK)
      {

         QMessageBox::critical(this, "failure on interface query", "You will not be able to use the features of this application");
         return(BAD_QUERY);
      }

   delegate1 = new DeckLinkCaptureDelegate();
   delegate1->setInterface(1);

   delegate1->setGrabFlag(false);
   deckLinkInput1->SetCallback(delegate1);
   delegate1->setDataDirectory(dataDirectory);

   numDevices++;

   result = deckLinkInput1->EnableVideoInput(bmdModeHD1080i5994, bmdFormat8BitYUV, bmdVideoInputEnableFormatDetection);
   result = deckLinkInput1->StartStreams();
   firstDeckLinkInput = deckLinkInput1;


   // Connect to the second DeckLink instance
   if (deckLinkIterator->Next(&deckLink2) != S_OK)
      {
         QMessageBox::critical(this, "This application requires a DeckLink dual PCI card.", "You will not be able to use the features of this application until a DeckLink PCI card is installed.");
         return(NO_SECOND_DECKLINK_BOARD);
      }

   if (deckLink2->QueryInterface(IID_IDeckLinkInput, (void**)&deckLinkInput2) != S_OK)
      {

         QMessageBox::critical(this, "failure on interface query", "You will not be able to use the features of this application");
         return(BAD_QUERY);
      }
   delegate2 = new DeckLinkCaptureDelegate();
   delegate2->setInterface(2);
   delegate2->setGrabFlag(false);
   delegate2->setDataDirectory(dataDirectory);

   deckLinkInput2->SetCallback(delegate2);
   numDevices++;

   result = deckLinkInput2->EnableVideoInput(bmdModeHD1080i5994, bmdFormat8BitYUV, 0);
   result = deckLinkInput2->StartStreams();
   secondDeckLinkInput = deckLinkInput2;
   foundBoards = true;


   return numDevices;



}

void HDGrabMainWindow::closeEvent(QCloseEvent *event)
{
   if (OKToClose())
      {
         close();
         advancedWidget->close();
         event->accept();
      }
   else
      {
         event->ignore();
      }
}

bool HDGrabMainWindow::OKToClose()
{

   int ret = QMessageBox::warning(this, 	tr("Application"),
                                  tr("You are quitting hdGrab.\n""Do you really want to   quit?"),
                                  QMessageBox::Yes | QMessageBox::Default,
                                  QMessageBox::No,
                                  QMessageBox::Cancel | QMessageBox::Escape);
   if (ret == QMessageBox::Yes)
      return true;
   else if (ret == QMessageBox::Cancel)
      return false;

   return false;
}
