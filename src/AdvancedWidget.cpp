#include "HdGrabMainWindow.h"
#include "AdvancedWidget.h"

AdvancedWidget::AdvancedWidget(QWidget *parent) :
   QWidget(parent)
{

   grabImage1CheckBox = new QCheckBox("grab image 1");
   grabImage2CheckBox = new QCheckBox("grab image 2");
   grabImage1CheckBox->setChecked(true);
   grabImage2CheckBox->setChecked(true);

   autoTimerCheckBox = new QCheckBox("use timed grabs");
   autoTimerCheckBox->setChecked(false);
   connect(autoTimerCheckBox,SIGNAL(clicked(bool)), this, SLOT(timerVisibilityFunction(bool)));

   interFrameInterval = new QLineEdit();
   intervalLabel = new QLabel("interval(sec)");
   intervalSet = new QPushButton("set");
   connect(intervalSet,SIGNAL(clicked()), this, SLOT(setInterval()));
   interFrameInterval->setValidator(new QIntValidator( interFrameInterval));
   connect(interFrameInterval, SIGNAL(editingFinished()), this, SLOT(doneWithInterval()));

   interFrameInterval->setVisible(false);
   intervalLabel->setVisible(false);
   intervalSet->setVisible(false);
   intervalSet->setEnabled(false);

   QVBoxLayout *theLayout = new QVBoxLayout(this);
   theLayout->addWidget(grabImage1CheckBox);
   theLayout->addWidget(grabImage2CheckBox);
   theLayout->addWidget(autoTimerCheckBox);
   theLayout->addWidget(intervalLabel);

   QHBoxLayout *intLayout = new QHBoxLayout;
   intLayout->addWidget(interFrameInterval);
   intLayout->addWidget(intervalSet);

   theLayout->addLayout(intLayout);


}

void AdvancedWidget::doneWithInterval()
{
   theGrabInterval = interFrameInterval->text().toInt();
   if(theGrabInterval > 0)
      {
         intervalSet->setEnabled(true);
      }

}

void AdvancedWidget::timerVisibilityFunction(bool amIVisible)
{
   interFrameInterval->setVisible(amIVisible);
   intervalLabel->setVisible(amIVisible);
   intervalSet->setVisible(amIVisible);
}


void AdvancedWidget::setInterval()
{
   mainWindow->setAutoGrabRate(theGrabInterval);

}
