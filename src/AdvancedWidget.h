#ifndef ADVANCEDWIDGET_H
#define ADVANCEDWIDGET_H




#include <QtWidgets>


class AdvancedWidget : public QWidget
{
   Q_OBJECT
public:
   explicit AdvancedWidget(QWidget *parent = 0);

private:
   QCheckBox      *grabImage1CheckBox;
   QCheckBox      *grabImage2CheckBox;
   QCheckBox      *autoTimerCheckBox;

   QLineEdit      *interFrameInterval;
   QLabel         *intervalLabel;
   QPushButton    *intervalSet;

   int            theGrabInterval;

   
signals:

private slots:
   void        setInterval();
   void        timerVisibilityFunction(bool amIVisible);
   void        doneWithInterval();
   
public slots:
   
};

#endif // ADVANCEDWIDGET_H
